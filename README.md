# hybrid.itermcolors

iTerm colors based on hybrid.vim's recommended terminal colours.

See [vim-hybrid’s page for more
information](https://github.com/w0ng/vim-hybrid).

