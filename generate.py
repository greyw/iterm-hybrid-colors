# -*- coding: utf-8 -*-
"""
Generate iterm scheme file.

Colors from: https://github.com/w0ng/vim-hybrid

Home page: https://bitbucket.org/greyw/iterm-hybrid-colors
"""

COLORS = {
    'Ansi 0': '282A2E',
    'Ansi 1': 'A54242',
    'Ansi 2': '8C9440',
    'Ansi 3': 'DE935F',
    'Ansi 4': '5F819D',
    'Ansi 5': '85678F',
    'Ansi 6': '5E8D87',
    'Ansi 7': '707880',
    'Ansi 8': '373B41',
    'Ansi 9': 'CC6666',
    'Ansi 10': 'B5BD68',
    'Ansi 11': 'F0C674',
    'Ansi 12': '81A2BE',
    'Ansi 13': 'B294BB',
    'Ansi 14': '8ABEB7',
    'Ansi 15': 'C5C8C6',
    'Foreground': 'C5C8C6',
    'Background': '1D1F21',
    'Bold': 'C5C8C6',
    'Selection': '373B41',
    'Selected Text': 'C5C8C6',
    'Cursor': 'De935F',
    'Cursor Text': '282A2E',
}

OPENNER_XML = """<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
"""

CLOSER_XML = """</dict>
</plist>
"""

COLOR_XML = """        <key>{name} Color</key>
        <dict>
                <key>Blue Component</key>
                <real>{blue}</real>
                <key>Green Component</key>
                <real>{green}</real>
                <key>Red Component</key>
                <real>{red}</real>
        </dict>
"""


def toreal(hexv):
    return int(hexv, 16) / 255.0


def main():
    print OPENNER_XML
    for name, hexv in COLORS.iteritems():
        repl = dict(
            name=name,
            red=toreal(hexv[0:2]),
            green=toreal(hexv[2:4]),
            blue=toreal(hexv[4:6])
        )
        print COLOR_XML.format(**repl)

    print CLOSER_XML


if __name__ == '__main__':
    main()

